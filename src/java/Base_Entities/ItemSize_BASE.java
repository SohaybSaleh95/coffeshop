package Base_Entities;
import java.util.ArrayList;
import java.util.Date;
import java.math.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import Util.DAO_Generator;

public class ItemSize_BASE implements java.io.Serializable {
 


//-----------------Variables ---------------
private String RowId; 
private Integer ItemId = 0; 
private String Size; 
private BigDecimal Price; 

    public ItemSize_BASE() {
    }

//--------- Getters and setters --------
 public String getRowId() {
return this.RowId;}




  public void setRowId(String RowId) {


this.RowId = RowId;}


 public Integer getItemId() {
return this.ItemId;}




  public void setItemId(Integer ItemId) {


this.ItemId = ItemId;}


 public String getSize() {
return this.Size;}




  public void setSize(String Size) {


this.Size = Size;}


 public BigDecimal getPrice() {
return this.Price;}


  public void setPrice(BigDecimal Price) {


this.Price = Price;}


 

}