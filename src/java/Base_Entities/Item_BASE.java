package Base_Entities;
import java.util.ArrayList;
import java.util.Date;
import java.math.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import Util.DAO_Generator;

public class Item_BASE implements java.io.Serializable {
 


//-----------------Variables ---------------
private String RowId; 
private Integer CategoryId = 0; 
private String Name; 

    public Item_BASE() {
    }

//--------- Getters and setters --------
 public String getRowId() {
return this.RowId;}




  public void setRowId(String RowId) {


this.RowId = RowId;}


 public Integer getCategoryId() {
return this.CategoryId;}




  public void setCategoryId(Integer CategoryId) {


this.CategoryId = CategoryId;}


 public String getName() {
return this.Name;}




  public void setName(String Name) {


this.Name = Name;}


 

}