/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author moham
 */
public class DAO_Generator {

    public static final String URL = "jdbc:mysql://localhost:3306/coffeshop?useUnicode=true&characterEncoding=utf-8&autoReconnect=true";
    public static final String USER = "root";
    public static final String PASS = "";
    public static Connection conn;

    public static String generateRowID() {
        String val = null;
        try {
            String uuID = UUID.randomUUID().toString();
            uuID = uuID.substring(uuID.length() / 2 + 1, uuID.length());
            val = uuID + "-" + (System.nanoTime());
        } catch (Exception ex) {
            val = "" + System.nanoTime();
        }
        val = val.substring(18, val.length());
        return val;
    }

    public static Connection getConnection() throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            if (conn == null || conn.isClosed()) {
                conn = DriverManager.getConnection(URL, USER, PASS);
            }
            return conn;
        } catch (SQLException ex) {
            throw new RuntimeException("Error connecting to the database", ex);
        }
    }

    private static String fix_column_name(String old_col_name) {
        String column_name = old_col_name.toLowerCase();
        String[] column_name_parts = column_name.split("_");
        column_name = "";
        for (String str : column_name_parts) {
            column_name += str.substring(0, 1).toUpperCase();
            column_name += str.substring(1);
        }
        return column_name;
    }

    public static void main(String[] args) {
        //generate Button
        try {
            ArrayList<String> tableNames = new ArrayList<>();
            ArrayList<String> objectNames = new ArrayList<>();
            Connection con = getConnection();
            DatabaseMetaData md = con.getMetaData();
            ResultSet rss = md.getTables(null, null, "%", null);
            while (rss.next()) {
                String ObjectName = "";
                String[] objNameBefore = rss.getString(3).split("_");
                for (int i = 0; i < objNameBefore.length; i++) {
                    ObjectName += Character.toUpperCase(objNameBefore[i].charAt(0)) + objNameBefore[i].substring(1);
                }
                tableNames.add(rss.getString(3));
                objectNames.add(ObjectName);

            }
            File projectFolder = new File("src/java/");
            if (!projectFolder.isDirectory()) {
                projectFolder.mkdir();
            }
            String BASE_DAOS_FILE = projectFolder.getAbsolutePath() + "/Base_DAOS";
            String DAOS_FILE = projectFolder.getAbsolutePath() + "/DAOS";
            String BASE_Entities_FILE = projectFolder.getAbsolutePath() + "/Base_Entities";
            String Entities_FILE = projectFolder.getAbsolutePath() + "/Entities";

            //generating Base Entities starts
            for (int j = 0; j < tableNames.size(); j++) {
                File file = new File(BASE_Entities_FILE);
                if (!file.isDirectory()) {
                    file.mkdir();
                }
                file = new File(BASE_Entities_FILE + "/" + objectNames.get(j) + "_BASE.java");
                String query = "select * from " + tableNames.get(j);//table name
                String ClassVariables = "";
                String ClassImports = "package Base_Entities;\n"
                        + "import java.util.ArrayList;\n"
                        + "import java.util.Date;\n"
                        + "import java.math.*;\n"
                        + "import java.util.HashSet;\n"
                        + "import java.util.List;\n"
                        + "import java.util.Set;\n"
                        + "import Util.DAO_Generator;\n"
                        + "\n"
                        + "public class " + objectNames.get(j) + "_BASE implements java.io.Serializable {\n \n";

                PreparedStatement stmt = null;
                stmt = con.prepareStatement(query);
                ResultSet rs = null;
                rs = stmt.executeQuery();
                ResultSetMetaData rsmd = null;
                rsmd = rs.getMetaData();
                for (int i = 0; i < rsmd.getColumnCount(); i++) {

                    String column_name = fix_column_name(rsmd.getColumnName(i + 1).toLowerCase());
                    System.out.println(column_name + " " + rsmd.getColumnTypeName(i + 1));
                    if (rsmd.getColumnTypeName(i + 1).contains("VARCHAR")) {
                        ClassVariables += "private String " + column_name + "; \n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DATE")) {
                        ClassVariables += "private Date " + column_name + "; \n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("INT")) {
                        ClassVariables += "private Integer " + column_name + " = 0; \n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("TINYINT")) {
                        ClassVariables += "private boolean " + column_name + "; \n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DECIMAL")) {
                        ClassVariables += "private BigDecimal " + column_name + "; \n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("LONGBLOB")) {
                        ClassVariables += "private byte[] " + column_name + "; \n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DOUBLE")) {
                        ClassVariables += "private Double " + column_name + " = 0.0; \n";
                    }
                }
                ClassVariables += "\n"
                        + "    public " + objectNames.get(j) + "_BASE() {\n"
                        + "    }\n";

                String gettersAndSetters = "";

                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    String column_name = fix_column_name(rsmd.getColumnName(i + 1));

                    if (rsmd.getColumnTypeName(i + 1).equals("VARCHAR")) {
                        gettersAndSetters += " public String get" + column_name + "() {\n"
                                + "return this." + column_name + ";}\n\n\n"
                                + "\n\n"
                                + "  public void set" + column_name + "(String " + column_name + ") {\n\n\n"
                                + "this." + column_name + " = " + column_name + ";}\n\n\n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("DATE")) {
                        gettersAndSetters += " public Date get" + column_name + "() {\n"
                                + "return this." + column_name + ";}\n\n\n"
                                + "\n\n"
                                + "  public void set" + column_name + "(Date " + column_name + ") {\n\n\n"
                                + "this." + column_name + " = " + column_name + ";}\n\n\n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("INT")) {

                        gettersAndSetters += " public Integer get" + column_name + "() {\n"
                                + "return this." + column_name + ";}\n\n\n"
                                + "\n\n"
                                + "  public void set" + column_name + "(Integer " + column_name + ") {\n\n\n"
                                + "this." + column_name + " = " + column_name + ";}\n\n\n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("TINYINT")) {

                        gettersAndSetters += " public boolean get" + column_name + "() {\n"
                                + "return this." + column_name + ";}\n\n\n"
                                + "\n\n"
                                + "  public void set" + column_name + "(boolean " + column_name + ") {\n\n\n"
                                + "this." + column_name + " = " + column_name + ";}\n\n\n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("LONGBLOB")) {
                        gettersAndSetters += " public byte[] get" + column_name + "() {\n"
                                + "return this." + column_name + ";}\n"
                                + "\n\n"
                                + "  public void set" + column_name + "(byte[] " + column_name + ") {\n\n\n"
                                + "this." + column_name + " = " + column_name + ";}\n\n\n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DOUBLE")) {
                        gettersAndSetters += " public Double get" + column_name + "() {\n"
                                + "return this." + column_name + ";}\n"
                                + "\n\n"
                                + "  public void set" + column_name + "(Double " + column_name + ") {\n\n\n"
                                + "this." + column_name + " = " + column_name + ";}\n\n\n";
                    }
                    
                    if (rsmd.getColumnTypeName(i + 1).contains("DECIMAL")) {
                        gettersAndSetters += " public BigDecimal get" + column_name + "() {\n"
                                + "return this." + column_name + ";}\n"
                                + "\n\n"
                                + "  public void set" + column_name + "(BigDecimal " + column_name + ") {\n\n\n"
                                + "this." + column_name + " = " + column_name + ";}\n\n\n";
                    }

                }
                gettersAndSetters += " \n\n}";
                String wholeFile = ClassImports + "\n" + "\n//-----------------Variables ---------------\n" + ClassVariables
                        + "\n//--------- Getters and setters --------\n" + gettersAndSetters;
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                writer.write(wholeFile);

                writer.close();
                rs.close();
                stmt.close();

            }
            //generating Base Entities ends

            //generating Entities starts
            for (int j = 0; j < tableNames.size(); j++) {
                File file = new File(Entities_FILE);

                if (!file.isDirectory()) {
                    file.mkdir();
                }
                file = new File(Entities_FILE + "/" + objectNames.get(j) + ".java");
                if (file.exists()) {
                    continue;
                }
                String ClassImports = "package Entities;\n"
                        + "import java.util.ArrayList;\n"
                        + "import java.util.Date;\n"
                        + "import java.util.HashSet;\n"
                        + "import Base_Entities." + objectNames.get(j) + "_BASE;\n"
                        + "import java.util.List;\n"
                        + "import java.util.Set;\n"
                        + "import Util.DAO_Generator;\n"
                        + "public class " + objectNames.get(j) + " extends " + objectNames.get(j) + "_BASE  {\n \n"
                        + ""
                        + "\n}";

                String wholeFile = ClassImports + "\n";
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                writer.write(wholeFile);
                writer.close();
            }
            //generating Entities ends

            //generating Base DAOS starts
            for (int j = 0; j < tableNames.size(); j++) {
                BufferedWriter writer = null;
                File file = new File(BASE_DAOS_FILE);
                if (!file.isDirectory()) {
                    file.mkdir();
                }
                file = new File(BASE_DAOS_FILE + "/" + objectNames.get(j) + "_DAO_BASE.java");
                String query = "select * from " + tableNames.get(j);//table name
                String ClassImports = "package Base_DAOS;\n"
                        + "\n"
                        + "import Entities." + objectNames.get(j) + ";\n"
                        + "import java.sql.Connection;\n"
                        + "import java.sql.Date;\n"
                        + "import java.sql.PreparedStatement;\n"
                        + "import java.sql.ResultSet;\n"
                        + "import java.sql.SQLException;\n"
                        + "import java.sql.Statement;\n"
                        + "import java.util.ArrayList;\n"
                        + "import java.math.*;\n"
                        + "import Util.DAO_Generator;\n"
                        + "import java.util.List;";
                String ClassName = "public class " + objectNames.get(j) + "_DAO_BASE" + " {";
                PreparedStatement stmt = con.prepareStatement(query);
                ResultSet rs = stmt.executeQuery();
                ResultSetMetaData rsmd = rs.getMetaData();
                String updateStatement = "  public void updateRecord(" + objectNames.get(j) + " record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {"
                        + " Connection con = DAO_Generator.getConnection();\n"
                        + "            PreparedStatement ps = con.prepareStatement(\"UPDATE " + rsmd.getTableName(1) + " SET";
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    updateStatement += " " + rsmd.getColumnName(i + 1) + " = ?";
                    if (i != rsmd.getColumnCount() - 1) {
                        updateStatement += ",";
                    }
                }
                updateStatement += " WHERE " + rsmd.getColumnName(1) + "=? \");\n";
                String Rs = "";
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    String column_name = fix_column_name(rsmd.getColumnName(i + 1).toLowerCase());

                    if (rsmd.getColumnTypeName(i + 1).equals("VARCHAR")) {
                        Rs += ("entity_record.set" + column_name + "(rs.getString(\"" + rsmd.getColumnName(i + 1) + "\")); \n");
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("DATE")) {
                        Rs += ("entity_record.set" + column_name + "(rs.getDate(\"" + rsmd.getColumnName(i + 1) + "\"));\n");
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("INT")) {
                        Rs += ("entity_record.set" + column_name + "(rs.getInt(\"" + rsmd.getColumnName(i + 1) + "\"));\n");
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("TINYINT")) {
                        Rs += ("entity_record.set" + column_name + "(rs.getBoolean(\"" + rsmd.getColumnName(i + 1) + "\"));\n");
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("LONGBLOB")) {
                        Rs += ("entity_record.set" + column_name + "(rs.getBytes(\"" + rsmd.getColumnName(i + 1) + "\"));\n");
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DOUBLE")) {
                        Rs += ("entity_record.set" + column_name + "(rs.getDouble(\"" + rsmd.getColumnName(i + 1) + "\"));\n");
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DECIMAL")) {
                        Rs += ("entity_record.set" + column_name + "(rs.getBigDecimal(\"" + rsmd.getColumnName(i + 1) + "\"));\n");
                    }
                    
                }
                String getRecord = " public " + objectNames.get(j) + " getRecord(String rowID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {" + objectNames.get(j) + " entity_record  = null;\n"
                        + "        Connection con = DAO_Generator.getConnection();\n"
                        + "        String query = \"select * from " + rsmd.getTableName(1) + " where " + rsmd.getColumnName(1) + " = '\" + rowID + \"'\";\n"
                        + "        PreparedStatement stmt = con.prepareStatement(query);\n"
                        + "        ResultSet rs = stmt.executeQuery();\n"
                        + "        while (rs.next()) {\n"
                        + "entity_record = new " + objectNames.get(j) + "();\n"
                        + Rs
                        + "        }\n"
                        + "       \n"
                        + "            rs.close();\n"
                        + "            stmt.close();\n"
                        + "            \n"
                        + "            return entity_record;\n}";
                String ListWithMyQuery = "public List<" + objectNames.get(j) + "> getList(String myQuery) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {\n";
                ListWithMyQuery += "  List<" + objectNames.get(j) + "> listOfRecords = new ArrayList<>();\n"
                        + "        Connection con = DAO_Generator.getConnection();\n"
                        + "        String query = \"select * from " + rsmd.getTableName(1) + " where \" + myQuery;\n"
                        + "        PreparedStatement stmt = con.prepareStatement(query);\n"
                        + "        ResultSet rs = stmt.executeQuery();\n"
                        + "        while (rs.next()) {\n";
                ListWithMyQuery += objectNames.get(j) + " entity_record = new " + objectNames.get(j) + "();\n"
                        + Rs
                        + "        listOfRecords.add(entity_record);\n }\n"
                        + "       \nrs.close();\n"
                        + "        stmt.close();\n"
                        + "        \n"
                        + "        return listOfRecords;\n}\n\n\n }";
                String ListWithoutMyQuery = "public List<" + objectNames.get(j) + "> getList() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {\n";
                ListWithoutMyQuery += "  List<" + objectNames.get(j) + "> listOfRecords = new ArrayList<>();\n"
                        + "        Connection con = DAO_Generator.getConnection();\n"
                        + "        String query = \"select * from " + rsmd.getTableName(1) + "\";\n"
                        + "        PreparedStatement stmt = con.prepareStatement(query);\n"
                        + "        ResultSet rs = stmt.executeQuery();\n"
                        + "        while (rs.next()) {\n";
                ListWithoutMyQuery += objectNames.get(j) + " entity_record = new " + objectNames.get(j) + "();\n"
                        + Rs
                        + "        listOfRecords.add(entity_record);\n }\n"
                        + "       \nrs.close();\n"
                        + "        stmt.close();\n"
                        + "        \n"
                        + "        return listOfRecords;\n}";
                String insert = "  public void addRecord(" + objectNames.get(j) + " record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {"
                        + " \n "
                        + "record.set" + fix_column_name(rsmd.getColumnName(1)) + "(DAO_Generator.generateRowID());\n"
                        + "Connection con = DAO_Generator.getConnection(); \n PreparedStatement ps = con.prepareStatement(\"INSERT INTO " + rsmd.getTableName(1) + " VALUES";
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    if (i == 0) {
                        insert += "(";
                    }

                    insert += "?";
                    if (i != rsmd.getColumnCount() - 1) {
                        insert += ",";
                    }
                }
                insert += ")\");\n";
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    String column_name = fix_column_name(rsmd.getColumnName(i + 1));
                    if (rsmd.getColumnTypeName(i + 1).equals("VARCHAR")) {
                        insert += "ps.setString(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("DATE")) {
                        insert += ""
                                + "if(record." + "get" + column_name + "() !=null){"
                                + "ps.setDate(" + (i + 1) + ", new java.sql.Date(record." + "get" + column_name + "().getTime()));}"
                                + " \n else{"
                                + "ps.setDate(" + (i + 1) + ",new java.sql.Date(new java.util.Date().getTime()));"
                                + "}\n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("INT")) {
                        insert += "ps.setInt(" + (i + 1) + ",record." + "get" + column_name + "()); \n";

                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("TINYINT")) {
                        insert += "ps.setBoolean(" + (i + 1) + ",record." + "get" + column_name + "()); \n";

                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("LONGBLOB")) {
                        insert += "ps.setBytes(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DOUBLE")) {
                        insert += "ps.setDouble(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DECIMAL")) {
                        insert += "ps.setBigDecimal(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                    }
                }
                int last = 0;
                for (int i = 0; i < rsmd.getColumnCount(); i++) {
                    String column_name = fix_column_name(rsmd.getColumnName(i + 1));
                    if (rsmd.getColumnTypeName(i + 1).equals("VARCHAR")) {
                        updateStatement += "ps.setString(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                        last = i + 1;
                    }

                    if (rsmd.getColumnTypeName(i + 1).equals("DATE")) {
                        updateStatement += ""
                                + "if (record." + "get" + column_name + "() != null) {"
                                + "ps.setDate(" + (i + 1) + ",new java.sql.Date(record." + "get" + column_name + "().getTime()));}"
                                + "\n"
                                + "else{"
                                + "ps.setDate(" + (i + 1) + ",new java.sql.Date(new java.util.Date().getTime()));"
                                + "}"
                                + " \n";
                        last = i + 1;
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("INT")) {
                        updateStatement += "ps.setInt(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                        last = i + 1;
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("TINYINT")) {
                        updateStatement += "ps.setBoolean(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                        last = i + 1;
                    }
                    if (rsmd.getColumnTypeName(i + 1).equals("LONGBLOB")) {
                        updateStatement += "ps.setBytes(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                        last = i + 1;
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DOUBLE")) {
                        updateStatement += "ps.setDouble(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                        last = i + 1;
                    }
                    if (rsmd.getColumnTypeName(i + 1).contains("DECIMAL")) {
                        updateStatement += "ps.setBigDecimal(" + (i + 1) + ",record." + "get" + column_name + "()); \n";
                        last = i + 1;
                    }
                }
                updateStatement += "ps.setString(" + (last + 1) + ",record." + "get" + fix_column_name(rsmd.getColumnName(1)) + "()); \n   int i = ps.executeUpdate();\n"
                        + "            ps.close();\n"
                        + "            \n}";
                String Delete = " public void deleteRecord(String record_ID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {"
                        + "   Connection con = DAO_Generator.getConnection();\n"
                        + "            Statement stmt = con.createStatement();\n"
                        + "\n"
                        + "            int i = stmt.executeUpdate(\"DELETE FROM " + rsmd.getTableName(1) + " WHERE " + rsmd.getColumnName(1) + "='\" + record_ID+\"'\");\n"
                        + "            stmt.close();\n"
                        + "            \n"
                        + "        }";
                insert += "\n" + " int i = ps.executeUpdate();\n"
                        + "        ps.close();\n"
                        + "        \n}";

                String wholeFile = ClassImports + "\n" + ClassName + "\n //-----GET RECORD METHOD----------- \n" + getRecord
                        + "\n//-----------------INSERT METHOD ---------------\n" + insert + "\n//--------- DELETE METHOD--------\n" + Delete
                        + "\n//--------- UPDATE METHOD--------\n" + updateStatement
                        + "\n//--------- LIST METHOD ALL RECORDS--------\n" + ListWithoutMyQuery
                        + "\n//--------- LIST METHOD WHERE STATEMENT--------\n" + ListWithMyQuery;
                writer = new BufferedWriter(new FileWriter(file));
                writer.write(wholeFile);
                writer.close();
                rs.close();
                stmt.close();

            } //generating Base DAOS ends

            //generating DAOS starts
            for (int j = 0; j < tableNames.size(); j++) {
                File file = new File(DAOS_FILE);

                if (!file.isDirectory()) {
                    file.mkdir();
                }
                file = new File(DAOS_FILE + "/" + objectNames.get(j) + "_DAO.java");
                if (file.exists()) {
                    continue;
                }
                String ClassImports = "package DAOS;\n"
                        + "import java.util.ArrayList;\n"
                        + "import java.util.Date;\n"
                        + "import java.util.HashSet;\n"
                        + "import Base_DAOS." + objectNames.get(j) + "_DAO_BASE;\n"
                        + "import java.util.List;\n"
                        + "import java.util.Set;\n"
                        + "public class " + objectNames.get(j) + "_DAO extends " + objectNames.get(j) + "_DAO_BASE  {\n \n"
                        + ""
                        + "\n}";

                String wholeFile = ClassImports + "\n";
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                writer.write(wholeFile);
                writer.close();
            }
            //generating DAOS ends
            conn.close();
        } catch (Exception ex) {
            Logger.getLogger(DAO_Generator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void execute(String sql) throws Exception{
        Connection conn = new DAO_Generator().getConnection();
        Statement stmt = conn.createStatement();
        stmt.execute(sql);
        stmt.close();
    }
}
