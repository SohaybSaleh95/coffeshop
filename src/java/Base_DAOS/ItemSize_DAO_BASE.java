package Base_DAOS;

import Entities.ItemSize;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.math.*;
import Util.DAO_Generator;
import java.util.List;
public class ItemSize_DAO_BASE {
 //-----GET RECORD METHOD----------- 
 public ItemSize getRecord(String rowID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {ItemSize entity_record  = null;
        Connection con = DAO_Generator.getConnection();
        String query = "select * from item_size where row_id = '" + rowID + "'";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
entity_record = new ItemSize();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setItemId(rs.getInt("item_id"));
entity_record.setSize(rs.getString("size")); 
entity_record.setPrice(rs.getBigDecimal("price"));
        }
       
            rs.close();
            stmt.close();
            
            return entity_record;
}
//-----------------INSERT METHOD ---------------
  public void addRecord(ItemSize record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { 
 record.setRowId(DAO_Generator.generateRowID());
Connection con = DAO_Generator.getConnection(); 
 PreparedStatement ps = con.prepareStatement("INSERT INTO item_size VALUES(?,?,?,?)");
ps.setString(1,record.getRowId()); 
ps.setInt(2,record.getItemId()); 
ps.setString(3,record.getSize()); 
ps.setBigDecimal(4,record.getPrice()); 

 int i = ps.executeUpdate();
        ps.close();
        
}
//--------- DELETE METHOD--------
 public void deleteRecord(String record_ID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {   Connection con = DAO_Generator.getConnection();
            Statement stmt = con.createStatement();

            int i = stmt.executeUpdate("DELETE FROM item_size WHERE row_id='" + record_ID+"'");
            stmt.close();
            
        }
//--------- UPDATE METHOD--------
  public void updateRecord(ItemSize record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { Connection con = DAO_Generator.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE item_size SET row_id = ?, item_id = ?, size = ?, price = ? WHERE row_id=? ");
ps.setString(1,record.getRowId()); 
ps.setInt(2,record.getItemId()); 
ps.setString(3,record.getSize()); 
ps.setBigDecimal(4,record.getPrice()); 
ps.setString(5,record.getRowId()); 
   int i = ps.executeUpdate();
            ps.close();
            
}
//--------- LIST METHOD ALL RECORDS--------
public List<ItemSize> getList() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<ItemSize> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from item_size";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
ItemSize entity_record = new ItemSize();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setItemId(rs.getInt("item_id"));
entity_record.setSize(rs.getString("size")); 
entity_record.setPrice(rs.getBigDecimal("price"));
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}
//--------- LIST METHOD WHERE STATEMENT--------
public List<ItemSize> getList(String myQuery) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<ItemSize> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from item_size where " + myQuery;
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
ItemSize entity_record = new ItemSize();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setItemId(rs.getInt("item_id"));
entity_record.setSize(rs.getString("size")); 
entity_record.setPrice(rs.getBigDecimal("price"));
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}


 }