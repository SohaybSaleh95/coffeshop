package Base_DAOS;

import Entities.Item;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.math.*;
import Util.DAO_Generator;
import java.util.List;
public class Item_DAO_BASE {
 //-----GET RECORD METHOD----------- 
 public Item getRecord(String rowID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {Item entity_record  = null;
        Connection con = DAO_Generator.getConnection();
        String query = "select * from item where row_id = '" + rowID + "'";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
entity_record = new Item();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setCategoryId(rs.getInt("category_id"));
entity_record.setName(rs.getString("name")); 
        }
       
            rs.close();
            stmt.close();
            
            return entity_record;
}
//-----------------INSERT METHOD ---------------
  public void addRecord(Item record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { 
 record.setRowId(DAO_Generator.generateRowID());
Connection con = DAO_Generator.getConnection(); 
 PreparedStatement ps = con.prepareStatement("INSERT INTO item VALUES(?,?,?)");
ps.setString(1,record.getRowId()); 
ps.setInt(2,record.getCategoryId()); 
ps.setString(3,record.getName()); 

 int i = ps.executeUpdate();
        ps.close();
        
}
//--------- DELETE METHOD--------
 public void deleteRecord(String record_ID) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {   Connection con = DAO_Generator.getConnection();
            Statement stmt = con.createStatement();

            int i = stmt.executeUpdate("DELETE FROM item WHERE row_id='" + record_ID+"'");
            stmt.close();
            
        }
//--------- UPDATE METHOD--------
  public void updateRecord(Item record) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException { Connection con = DAO_Generator.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE item SET row_id = ?, category_id = ?, name = ? WHERE row_id=? ");
ps.setString(1,record.getRowId()); 
ps.setInt(2,record.getCategoryId()); 
ps.setString(3,record.getName()); 
ps.setString(4,record.getRowId()); 
   int i = ps.executeUpdate();
            ps.close();
            
}
//--------- LIST METHOD ALL RECORDS--------
public List<Item> getList() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Item> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from item";
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Item entity_record = new Item();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setCategoryId(rs.getInt("category_id"));
entity_record.setName(rs.getString("name")); 
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}
//--------- LIST METHOD WHERE STATEMENT--------
public List<Item> getList(String myQuery) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
  List<Item> listOfRecords = new ArrayList<>();
        Connection con = DAO_Generator.getConnection();
        String query = "select * from item where " + myQuery;
        PreparedStatement stmt = con.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
Item entity_record = new Item();
entity_record.setRowId(rs.getString("row_id")); 
entity_record.setCategoryId(rs.getInt("category_id"));
entity_record.setName(rs.getString("name")); 
        listOfRecords.add(entity_record);
 }
       
rs.close();
        stmt.close();
        
        return listOfRecords;
}


 }